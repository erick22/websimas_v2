from django import forms
from .models import Catblog

class TagForm(forms.Form):
	tag = forms.ModelChoiceField(queryset=Catblog.objects.all(),
		widget=forms.Select(attrs={'class':'dropdownblog'}))
