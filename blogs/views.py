from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
from blogs.models import *
from blogs.forms import TagForm
from otras.models import MenuFoto

# Create your views here.
class ListBlogsView(ListView):
    model = Blog
    queryset = Blog.objects.order_by('-fecha')
    # paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ListBlogsView, self).get_context_data(**kwargs)
        context['form_tag'] = TagForm()
        context['tags'] = Catblog.objects.all()
        context['categorias'] = Blog.objects.values_list('idcatblog__id','idcatblog__catblog').distinct('idcatblog').exclude(idcatblog = None)
        # context['foto_blog'] = MenuFoto.objects.get(menu=5)

        return context

def blogs_categoria(request,template='blogs/blog_list.html',id=None):
	object_list = Blog.objects.filter(idcatblog__id=id)
	categorias = Blog.objects.values_list('idcatblog__id','idcatblog__catblog').distinct('idcatblog').exclude(idcatblog = None)
	return render(request, template, locals())

def DetailBlogsView(request, template='blogs/detalle_blog.html', id=None, uri=None):
    nota = get_object_or_404(Blog, id=id)

    lista_tematicas = nota.idcatblog

    try:
        noticias_relacionadas = Blog.objects.filter(idcatblog=lista_tematicas).exclude(id=nota.id).order_by('-id')[:3]
    except:
        pass

    # foto_blog = MenuFoto.objects.get(menu=5)

    return render(request, template, locals())


def blogs_filtrados(request, template="blogs/blog_list.html"):
    object_list = Blog.objects.filter(idcatblog=request.POST["tag"])
    tags = Catblog.objects.all()
    foto_blog = MenuFoto.objects.get(menu=5)
    return render(request, template, locals())

from haystack.query import SearchQuerySet
from haystack.views import SearchView, search_view_factory
from haystack.forms import ModelSearchForm
def search_blogs(request):
	sqs = SearchQuerySet().models(Blog).order_by('-id')
	view = search_view_factory(
		view_class=SearchView,
		template='search/search_blogs.html',
		searchqueryset=sqs,
		form_class=ModelSearchForm,
		results_per_page=10
		)
	return view(request)
