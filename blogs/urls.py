from django.urls import path, include

from .views import ListBlogsView, DetailBlogsView, blogs_filtrados, blogs_categoria

app_name = 'blogs'

urlpatterns = [
    path('blogs/',ListBlogsView.as_view(), name='blogs'),
   	path('blogs/<int:id>/<str:uri>/', DetailBlogsView, name='detalle-blog'),
    path('blogs/filtrar/', blogs_filtrados, name='blog-filtro'),
    path('blogs/categoria/<int:id>', blogs_categoria, name = "list-blogs-cat")

]


#  patterns('blogs.views',
#     url(r'^blogs/$', ListBlogsView.as_view(), name='blogs'),
#     url(r'^blogs/(?P<id>[0-9]+)/(?P<uri>[-_\w]+)/$', 'DetailBlogsView',
#     									name='detalle-blog'),
#     url(r'^blogs/filtrar/$', 'blogs_filtrados', name='blog-filtro'),

# )
