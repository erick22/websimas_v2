from haystack import indexes
from .models import Blog
from django.db.models import Q

class BlogIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    blog = indexes.NgramField(model_attr='blog', null=True)
    resumen = indexes.NgramField(model_attr='resumen', null=True)

    def get_model(self):
        return Blog

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.order_by('-id')
