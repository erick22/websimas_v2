from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.template.defaultfilters import slugify
from sorl.thumbnail import ImageField, get_thumbnail
from websimas.utils import get_file_path
from django.contrib.gis.db import models as geo_models

# Create your models here.
class Departamento(models.Model):
	nombre = models.CharField(max_length=350)
	codigo = models.CharField(max_length=10)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Departamentos"

CAT_CHOICES = (('proyectos-recientes','Proyectos Recientes'),('experiencia','Experiencia'))

class Proyecto(models.Model):
	nombre = models.CharField(max_length=350)
	nombre_corto = models.CharField(max_length=50)
	foto = ImageField(upload_to=get_file_path)
	resumen = RichTextUploadingField()
	#departamento = models.ManyToManyField(Departamento)
	beneficiarios = models.IntegerField(blank=True, null=True)
	slug = models.SlugField(max_length=350,editable=False)
	fileDir = 'proyectos/'
	thumb = models.CharField(max_length=500,editable=False)
	ubicacion_proyecto = geo_models.MultiPointField(null=True,blank=True)
	categoria = models.CharField(choices=CAT_CHOICES, max_length=20)

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		self.thumb = get_thumbnail(self.foto, '104x104', crop='center', quality=99)
		return super(Proyecto, self).save(*args, **kwargs)

	class Meta:
		verbose_name_plural = "Proyectos"

	@property
	def get_thumb(self):
		im = get_thumbnail(self.foto, '104x104', crop='center', quality=99)
		return im.url

class FotosProyecto(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    archivo = ImageField(upload_to=get_file_path)
    credito = models.CharField(max_length=200, blank=True, null=True)
    fileDir = 'proyectos/img/'

    class Meta:
        verbose_name_plural = "Fotos"
