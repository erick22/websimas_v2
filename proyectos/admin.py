from django.contrib import admin
from .models import *
from django.contrib.gis import admin

# Register your models here.

class FotosProyectoInline(admin.TabularInline):
	model = FotosProyecto
	extra = 1

from leaflet.admin import LeafletGeoAdminMixin

class ProyectoAdmin(LeafletGeoAdminMixin,admin.ModelAdmin):
	# filter_horizontal = ('departamento',)
	inlines = [FotosProyectoInline,]
	# form = MapForm

admin.site.register(Proyecto,ProyectoAdmin)
admin.site.register(Departamento)


