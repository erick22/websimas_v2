from django.shortcuts import render
from .models import *

# Create your views here.
def list_proyectos(request,template='proyectos/list_proyectos.html'):
	object_list = Proyecto.objects.filter(categoria = 'proyectos-recientes').order_by('-id')
	categorias = CAT_CHOICES
	return render(request, template, locals())

def detalle_proyecto(request,template='proyectos/proyecto_detalle.html',slug=None):
	object = Proyecto.objects.get(slug = slug)
	return render(request, template, locals())

def filtro_proyecto(request,template='proyectos/list_proyectos.html',categoria=None):
	object_list = Proyecto.objects.filter(categoria = categoria).order_by('-id')
	categorias = CAT_CHOICES
	return render(request, template, locals())
