from django.db import models
from publicaciones.models import Tematica
from django.template.defaultfilters import slugify
from sorl.thumbnail import ImageField
from websimas.utils import get_file_path

# Create your models here.

class Evento(models.Model):
    evento = models.CharField(max_length=400)
    objetivo = models.TextField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    lugar = models.CharField(max_length=400, blank=True, null=True)
    contacto = models.CharField(max_length=200, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    telefono = models.CharField(max_length=35, blank=True, null=True)
    fax = models.CharField(max_length=35, blank=True, null=True)
    apartado = models.CharField(max_length=25, blank=True, null=True)
    correo = models.CharField(max_length=150, blank=True, null=True)
    web = models.CharField(max_length=160, blank=True, null=True)
    fechaingreso = models.DateField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True, null=True)
    logotipo = ImageField('logo Evento', upload_to=get_file_path,blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    organiza = models.CharField(max_length=200, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True)
    ciudad = models.CharField(max_length=200, blank=True, null=True)
    idtematica = models.ManyToManyField(Tematica, verbose_name="Tematica", blank=True)

    fileDir = 'logoEventos/'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.evento))
        super(Evento, self).save(*args, **kwargs)


    class Meta:
        verbose_name_plural = "Eventos"

    def __str__(self):
        return self.evento

