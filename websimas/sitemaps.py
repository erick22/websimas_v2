from noticias.models import Noticia
from blogs.models import Blog
from publicaciones.models import Publicacion
from django.contrib.sitemaps import Sitemap, views

class NoticiaSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return Noticia.objects.exclude(uri = '')

    def lastmod(self, obj):
        return obj.fecha

class PublicacionSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return Publicacion.objects.exclude(uri = '')

    def lastmod(self, obj):
        return obj.fecha

class BlogSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return Blog.objects.exclude(uri = '')

    def lastmod(self, obj):
        return obj.fecha

sitemaps = {
    'noticias': NoticiaSitemap,
    # 'publicaciones': PublicacionSitemap,
    # 'blogs': BlogSitemap,
}