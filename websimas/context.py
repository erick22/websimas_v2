from noticias.models import *
from publicaciones.models import *
from otras.models import *
from proyectos.models import *
from django import forms

class BuscadorForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super(BuscadorForm, self).__init__(*args, **kwargs)
		self.fields['q'] = forms.CharField()

def globales(request):
	cat_notas = Noticia.objects.values_list('categoria__slug','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	cat_publi = Publicacion.objects.values_list('idcategoria__slug','idcategoria__nombre').distinct('idcategoria__id').exclude(idcategoria__nombre = None).order_by('idcategoria__id')

	search = BuscadorForm()

	servicio = Servicio.objects.filter(activo = True)[:1].get()
	proyectos = Proyecto.objects.order_by('-id')

	junta_directiva = Trabajadores.objects.filter(ubicacion__ubicacion = 'Junta Directiva')

	latest_news_footer = Noticia.objects.order_by('-fecha')[:3]

	return {'cat_notas':cat_notas, 'cat_publi': cat_publi, 'search': search, 'servicio': servicio, 'proyectos': proyectos,
			'junta_directiva': junta_directiva, 'latest_news_footer':latest_news_footer}
