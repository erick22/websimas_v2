# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from .models import Publicacion
from otras.models import MenuFoto

# Create your views here.


def ListBooksView(request, template='publicaciones/publicacion_list.html'):
    object_list = Publicacion.objects.filter(iddisponibilidad=4).order_by('-id')
    # context['foto_publi'] = MenuFoto.objects.get(menu=2)
    tematicas = Publicacion.objects.values_list('idcategoria__id','idcategoria__nombre').order_by('idcategoria__id').distinct('idcategoria__id').exclude(idcategoria__nombre = None)

    return render(request, template, locals())

def FiltroListBooksView(request,id,template='publicaciones/publicacion_list.html'):
    object_list = Publicacion.objects.filter(iddisponibilidad=4,idcategoria__id=id).order_by('-fecha')
    tematicas = Publicacion.objects.values_list('idcategoria__id','idcategoria__nombre').order_by('idcategoria__id').distinct('idcategoria__id').exclude(idcategoria__nombre = None)

    return render(request, template, locals())

def DetailBooksView(request, template='publicaciones/detalle_publicacion.html',id=None, uri=None):
    publi = get_object_or_404(Publicacion, id=id)
    # foto_publi = MenuFoto.objects.get(menu=2)
    return render(request, template, locals())


class ListBookSimasView(ListView):
    model = Publicacion
    queryset = Publicacion.objects.filter(cidoc=False,iddisponibilidad=4).order_by('-fecha')
    # paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ListBookSimasView, self).get_context_data(**kwargs)
        context['foto_publi'] = MenuFoto.objects.get(menu=2)

        return context


class ListBookCidocView(ListView):
    model = Publicacion
    queryset = Publicacion.objects.filter(cidoc=True,iddisponibilidad=4).order_by('-id')
    # paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(ListBookCidocView, self).get_context_data(**kwargs)
        context['foto_publi'] = MenuFoto.objects.get(menu=2)

        return context

def publicaciones_categoria(request,template='publicaciones/publicacion_list.html',slug=None):
    object_list = Publicacion.objects.filter(idcategoria__slug=slug,iddisponibilidad=4)

    return render(request, template, locals())

from haystack.query import SearchQuerySet
from haystack.views import SearchView, search_view_factory
from haystack.forms import ModelSearchForm
def search_publicacion(request):
    sqs = SearchQuerySet().models(Publicacion).order_by('-id')
    view = search_view_factory(
        view_class=SearchView,
        template='search/search_publicacion.html',
        searchqueryset=sqs,
        form_class=ModelSearchForm,
        results_per_page=16
        )
    return view(request)
