# Generated by Django 2.2.12 on 2020-06-19 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('otras', '0004_auto_20200528_0847'),
        ('publicaciones', '0003_auto_20200526_1114'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='publicacion',
            name='idtematica',
        ),
        migrations.RemoveField(
            model_name='publicacion',
            name='idcategoria',
        ),
        migrations.AddField(
            model_name='publicacion',
            name='idcategoria',
            field=models.ManyToManyField(blank=True, to='otras.Categoria', verbose_name='Categoria'),
        ),
    ]
