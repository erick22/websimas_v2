from haystack import indexes
from .models import Publicacion
from django.db.models import Q

class PublicacionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    publicacion = indexes.NgramField(model_attr='publicacion',null=True)
    idcategoria = indexes.MultiValueField(model_attr='idcategoria__nombre',null=True)

    def get_model(self):
        return Publicacion

    def prepare_idcategoria(self, object):
        return [categoria.nombre for categoria in object.idcategoria.all()]

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(iddisponibilidad=4).order_by('-id')
