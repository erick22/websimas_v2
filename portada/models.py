# -*- coding: utf-8 -*-
from django.db import models
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField
from .fields import OrderField

# Create your models here.

class FotoPortada(models.Model):
    texto1 = models.CharField(max_length=250)
    texto2 = models.CharField(max_length=250)
    foto = ImageField(upload_to=get_file_path)
    url = models.URLField(blank=True,null=True)
    orden = OrderField(blank=True)

    fileDir = 'portada/'

    def __str__(self):
        return self.texto1
