from django.db import models

# Create your models here.
class Correo(models.Model):
	email = models.EmailField(unique=True)

	class Meta:
		verbose_name_plural = "Correos"

	def __str__(self):
		return self.email