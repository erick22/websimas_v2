from django.urls import path
from .views import *

urlpatterns = [
	path('galerias/', GaleriaImgListView, name = "list-galeria"),
	path('galerias/filtro/<id>', filtro_galeria, name = "filtro-galeria"),
	path('galerias/videos/', videos, name="videos"),
	path('galerias/videos/<id>', videos_detalle, name="videos-detalle"),
	path('galerias/imagenes/', imagenes, name="imagenes"),
	path('galerias/imagenes/<slug>', detalle_galeria, name = "detalle-galeria"),
]
