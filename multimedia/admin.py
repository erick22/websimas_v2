from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin
from django.db.models import Q

class YoutubeListFilter(admin.SimpleListFilter):

    title = ('Tiene url de youtube')

    parameter_name = 'has_url'

    def lookups(self, request, model_admin):

        return (
            ('si', ('Si')),
            ('no',  ('No')),
        )

    def queryset(self, request, queryset):

        if self.value() == 'si':
            return queryset.filter(youtube__isnull=False).exclude(youtube='')

        if self.value() == 'no':
            return queryset.filter(Q(youtube__isnull=True) | Q(youtube__exact=''))

class VideoAdmin(ImportExportModelAdmin):
	list_display = ('video','codigo','texto','realizacion')
	search_fields = ('video','codigo','texto')

	fields = (('video', 'codigo'), 'texto',('youtube','googleid'), ('claves','cidoc'),
		('duracion','anho'),('realizacion','pais'),'categoria','estado')

	list_filter = [YoutubeListFilter]


# Register your models here.
admin.site.register(Cataudio)
admin.site.register(Audio)
admin.site.register(Estadovideo)
admin.site.register(Video, VideoAdmin)

class FotosGaleriaInline(admin.TabularInline):
	model = FotosGaleria
	extra = 1

class GaleriasImgAdmin(admin.ModelAdmin):
	inlines = [FotosGaleriaInline,]

admin.site.register(GaleriasImg, GaleriasImgAdmin)
