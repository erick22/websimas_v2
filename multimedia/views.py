from django.shortcuts import render
from .models import *
from django.views.generic import ListView, DetailView
from otras.models import *
from publicaciones.models import *
from itertools import chain
# Create your views here.

def GaleriaImgListView(request,template='galeria/galeria.html'):

	imagenes = GaleriasImg.objects.order_by('-id')
	videos = Video.objects.exclude(youtube__exact='').order_by('-id')
	object_list = list(chain(imagenes,videos))

	tematica_img = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas_videos = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)

	tematicas = list(set(chain(tematica_img,tematicas_videos)))
	try:
		foto_menu = MenuFoto.objects.get(menu=8)
	except:
		pass

	return render(request, template, locals())

def filtro_galeria(request,id,template='galeria/galeria.html'):
	imagenes = GaleriasImg.objects.filter(categoria = id).order_by('-id')
	videos = Video.objects.filter(categoria = id).exclude(youtube__exact='').order_by('-id')
	object_list = list(chain(imagenes,videos))

	tematica_img = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas_videos = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas = list(set(chain(tematica_img,tematicas_videos)))
	return render(request, template, locals())

def detalle_galeria(request,slug,template='galeria/detalle.html'):
	object = GaleriasImg.objects.get(slug = slug)
	return render(request, template, locals())

def videos(request,template='galeria/galeria.html'):
	object_list = Video.objects.exclude(youtube__exact='').order_by('-id')
	tematica_img = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas_videos = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas = list(set(chain(tematica_img,tematicas_videos)))
	return render(request, template, locals())

def videos_detalle(request,id,template='galeria/detalle.html'):
	object = Video.objects.get(id = id)
	return render(request, template, locals())

def imagenes(request,template='galeria/galeria.html'):
	object_list = GaleriasImg.objects.order_by('-id')
	tematica_img = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas_videos = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	tematicas = list(set(chain(tematica_img,tematicas_videos)))
	return render(request, template, locals())
