# -*- coding: utf-8 -*-
from django.db import models
from publicaciones.models import Tematica
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField
from ckeditor_uploader.fields import RichTextUploadingField
from django.template.defaultfilters import slugify
from noticias.models import Categoria
from embed_video.fields import EmbedVideoField
# Create your models here.

class Cataudio(models.Model):
    #idcataudio = models.IntegerField(primary_key=True)
    cataudio = models.CharField(max_length=250, blank=True)
    ordencataudio = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.cataudio

class Audio(models.Model):
    #idaudio = models.IntegerField(primary_key=True)
    audio = models.CharField(max_length=255, blank=True)
    embed = models.TextField(blank=True)
    archivomp3 = models.CharField(max_length=500, blank=True)
    fecha_ini = models.DateField(blank=True, null=True)
    idcataudio = models.ForeignKey('Cataudio', blank=True, null=True, on_delete= models.CASCADE)

    def __str__(self):
        return self.audio


class Estadovideo(models.Model):
    #idestadovideo = models.IntegerField(primary_key=True)
    estadovideo = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.estadovideo

class Video(models.Model):
    #idvideo = models.IntegerField(primary_key=True)
    video = models.CharField(max_length=255, blank=True, null=True)
    texto = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=300, blank=True, null=True)
    claves = models.CharField(max_length=255, blank=True, null=True)
    creacion = models.DateTimeField(blank=True, null=True)
    archivo = models.CharField(max_length=500, blank=True, null=True)
    idusuario = models.IntegerField(blank=True, null=True)
    preview = models.CharField(max_length=500, blank=True, null=True)
    youtube = EmbedVideoField(blank=True, null=True)
    idtematica = models.IntegerField(blank=True, null=True)
    cidoc = models.NullBooleanField()
    codigo = models.CharField(max_length=50, blank=True, null=True)
    duracion = models.CharField(max_length=50, blank=True, null=True)
    anho = models.CharField('Año', max_length=4, blank=True, null=True)
    realizacion = models.CharField(max_length=255, blank=True, null=True)
    #idestadovideo = models.ForeignKey(blank=True, null=True)
    pais = models.CharField(max_length=50, blank=True, null=True)
    googleid = models.CharField(max_length=500, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    titulo2 = models.CharField(max_length=300, blank=True, null=True)
    # tematica = models.ForeignKey(Tematica, null=True, blank=True, on_delete=models.CASCADE)
    estado = models.ForeignKey(Estadovideo, verbose_name='Estado del video',blank=True, null=True,on_delete=models.CASCADE)
    categoria = models.ManyToManyField(Categoria,blank=True)
   
    def __str__(self):
        return self.video

RES_CHOICES = ((1,'Vertical 800x960'),(2,'Horizontal 1600x480'),(3,'Normal 800x480'))

class GaleriasImg(models.Model):
    titulo = models.CharField(max_length=255)
    portada = ImageField(upload_to=get_file_path)
    credito = models.CharField(max_length=200, blank=True, null=True)
    fecha = models.DateField()
    descripcion = RichTextUploadingField(blank=True, null=True)
    # tematica = models.ManyToManyField(Tematica)
    categoria = models.ManyToManyField(Categoria,blank=True)
    #resolucion_index = models.IntegerField(choices=RES_CHOICES)
    slug = models.SlugField(max_length=255,editable=False)

    fileDir = 'galerias/img/portada/'

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        return super(GaleriasImg, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Galerias de Imagenes"

class FotosGaleria(models.Model):
    galeria = models.ForeignKey(GaleriasImg, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    archivo = ImageField(upload_to=get_file_path)
    credito = models.CharField(max_length=200, blank=True, null=True)
    fileDir = 'galerias/img/'

    class Meta:
        verbose_name_plural = "Fotos"