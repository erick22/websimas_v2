# -*- coding: utf-8 -*-
from django.db import models
from fotos.models import *
from django.contrib.contenttypes.fields import GenericRelation
from otras.models import *
from django.template.defaultfilters import slugify

# Create your models here.
class Organizaciones(models.Model):
    nombre = models.CharField(max_length=250)
    iniciales = models.CharField(max_length=150)

    def __str__(self):
        return self.iniciales

TIPO_CHOICES = ((1,'Pagina web'),(2,'Aplicación movil'),(3,'Sistema web'))

class Portafolio(models.Model):
    idservicio = models.ForeignKey(Servicio,on_delete = models.CASCADE)
    servicio = models.CharField('Nombre del servicio', max_length=250)
    resumen = models.TextField()
    fecha = models.DateField('Fecha de realización')
    link = models.URLField(blank=True)
    organizacion = models.ManyToManyField(Organizaciones)
    tipo =  models.IntegerField(choices=TIPO_CHOICES,blank=True,null=True)
    slug = models.SlugField(max_length=250,editable=False)
    fotos = GenericRelation(Fotos)

    def __str__(self):
        return self.servicio
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.servicio)
        super(Portafolio, self).save(*args, **kwargs)

