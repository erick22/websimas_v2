#encoding: utf-8

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from sorl.thumbnail import ImageField
from websimas.utils import get_file_path
from django.contrib.auth.models import User

# Create your models here.

class Fotos(models.Model):
	nombre = models.CharField(max_length=150)
	imagen = ImageField(upload_to=get_file_path, blank=True, null=True)
	content_type = models.ForeignKey(ContentType,  on_delete=models.CASCADE)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')

	fileDir = 'fotos/'

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Fotos"
