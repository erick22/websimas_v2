from django.apps import AppConfig


class GoogleSearchConfig(AppConfig):
    name = 'googlesearch'
