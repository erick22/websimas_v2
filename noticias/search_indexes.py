from haystack import indexes
from .models import Noticia
from django.db.models import Q

class NoticiaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    noticia = indexes.NgramField(model_attr='noticia')
    resumen = indexes.NgramField(model_attr='resumen',null=True)

    def get_model(self):
        return Noticia

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.order_by('-id')
