from django.contrib.syndication.views import Feed
from django.urls import reverse
from noticias.models import Noticia

class NewsFeed(Feed):
   title = "Noticias SIMAS"
   link = "/rss-simas/"
   description = "Ultimas noticias"

   def items(self):
      return Noticia.objects.all().order_by("-fecha")[:10]
		
   def item_title(self, item):
      return item.noticia
		
   def item_description(self, item):
      return item.descripcion
		
   def item_link(self, item):
      return reverse('noticias:detalle-noticia',args=[item.id, item.uri])