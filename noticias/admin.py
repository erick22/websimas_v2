from django.contrib import admin
from .models import *


class NoticiasAdmin(admin.ModelAdmin):
    filter_horizontal = ('categoria',)
    date_hierarchy = 'fecha'
    list_display = ('id', 'noticia', 'fecha','idautor','categorias')
    list_display_links = ('id', 'noticia')
    list_filter = ('categoria','idautor',)
    search_fields = ('noticia','idautor','categoria')
    fieldsets = (
        (None, {
            'fields': (('noticia', 'fecha'),'descripcion', ('foto','credito'),'url','uri','texto',('tipo','categoria'),
                       ('importante','claves','idautor'))
        }),
        ('Archivos para adjuntar', {
            'fields': (('titarchivo1', 'archivo1'), ('titarchivo2','archivo2'),
                      ('archivo', 'archivo3'))
        }),
    )
# Register your models here.
admin.site.register(Noticia, NoticiasAdmin)
