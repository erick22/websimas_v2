# Generated by Django 2.2.12 on 2020-06-19 16:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('otras', '0004_auto_20200528_0847'),
        ('noticias', '0004_auto_20200525_1245'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='noticia',
            name='idtematica',
        ),
        migrations.RemoveField(
            model_name='noticia',
            name='categoria',
        ),
        migrations.AddField(
            model_name='noticia',
            name='categoria',
            field=models.ManyToManyField(blank=True, to='otras.Categoria'),
        ),
    ]
