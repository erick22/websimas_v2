from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from django.views.generic import ListView, DetailView
import json
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
#from django.views.generic.detail import DetailView
from .models import Noticia
from .forms import ContactForm
from publicaciones.models import Publicacion
from blogs.models import Blog
from portafolio.models import Portafolio
from portada.models import FotoPortada
from historia.models import Historias
from otras.models import *
from contacto.models import *
from multimedia.models import *
from proyectos.models import *
#from bs4 import BeautifulSoup, Tag
#from endless_pagination.decorators import page_template
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json as simplejson
from django.db.models import Sum
from django.forms import ModelForm
from suscriptores.models import *
from itertools import chain

####### mailchimp conf############
from django.http import HttpResponse, JsonResponse
import threading
import mailchimp
from django.conf import settings

class SendSubscribeMail(object):
	def __init__(self, email):
		self.email = email
		thread = threading.Thread(target=self.run, args=())
		thread.daemon = True
		thread.start()

	def run(self):
		API_KEY = settings.MAILCHIMP_API_KEY
		LIST_ID = settings.MAILCHIMP_SUBSCRIBE_LIST_ID
		api = mailchimp.Mailchimp(API_KEY)
		try:
			api.lists.subscribe(LIST_ID, {'email': self.email})
		except:
			return False

def subscribe(request):
	if request.method == 'POST':
		email = request.POST['email_id']
		email_qs = Correo.objects.filter(email = email)
		if email_qs.exists():
			data = {"status" : "404"}
			return JsonResponse(data)
		else:
			Correo.objects.create(email = email)
			SendSubscribeMail(email) # Send the Mail, Class available in utils.py

	return HttpResponse("/")
####### mailchimp conf############

#class PortafolioPageView(TemplateView):
#    template_name = "portafolio.html"

#    def get_context_data(self, **kwargs):
#        context = super(PortafolioPageView, self).get_context_data(**kwargs)
#        context['portafolios'] = Portafolio.objects.order_by('-fecha')

#        return context

def list_portfolio(request,template="portafolio/list_portafolio.html"):
	object_list =  Portafolio.objects.order_by('-fecha')
	tipos = Portafolio.objects.values_list('tipo',flat=True).distinct('tipo')
	return render(request, template, locals())

def detalle_portafolio(request,slug=None,template="portafolio/detalle_portafolio.html"):
	object = Portafolio.objects.get(slug = slug)
	return render(request, template, locals())

def filtro_portafolio(request,id=None,template="portafolio/list_portafolio.html"):
	object_list = Portafolio.objects.filter(tipo=id)
	tipos = Portafolio.objects.values_list('tipo',flat=True).distinct('tipo')
	return render(request, template, locals())

class ListServiceView(ListView):
	model = Servicio
	queryset = Servicio.objects.all()
	paginate_by = 4

	def get_context_data(self, **kwargs):
		context = super(ListServiceView, self).get_context_data(**kwargs)
		context['foto_portafolio'] = MenuFoto.objects.get(menu=3)

		return context

# def portafolio_serv_view(request, template='portafolio_serv.html', extra_context=None):
#     servicios = Servicio.objects.all()
#     context = {
#         'portafolios': Portafolio.objects.order_by('-id'),
#         'foto_portafolio': MenuFoto.objects.get(menu=3),
#         'servicios' : servicios,
#     }
#     if extra_context is not None:
#         context.update(extra_context)

#     return render(request, template, context)


class HistoriaPageView(TemplateView):
	template_name = "historia.html"

	def get_context_data(self, **kwargs):
		context = super(HistoriaPageView, self).get_context_data(**kwargs)
		context['historias'] = Historias.objects.all()
		context['foto_historia'] = MenuFoto.objects.get(menu=7)

		return context


class QuienesPageView(TemplateView):
	template_name = "quienesomos.html"

	def get_context_data(self, **kwargs):
		context = super(QuienesPageView, self).get_context_data(**kwargs)
		context['foto_quienes'] = MenuFoto.objects.get(menu=1)

		return context

class ContactenosPageView(TemplateView):
	template_name = "contacto.html"

	def get_context_data(self, **kwargs):
		context = super(ContactenosPageView, self).get_context_data(**kwargs)
		context['form_contact'] = ContactForm()
		context['ubicacion_trabajador'] = UbicacionTrabajor.objects.exclude(ubicacion = 'Junta Directiva').order_by('orden')
		context['personal'] = Trabajadores.objects.exclude(ubicacion__ubicacion = 'Junta Directiva')
		context['foto_contacto'] = MenuFoto.objects.get(menu=6)
		context['contacto'] = Contacto.objects.get()

		return context


class SuscripcionForm(ModelForm):
	class Meta:
		model = Correo
		fields = ('email',)

def HomePageView(request, template="index.html"):

	latest_news = Noticia.objects.order_by('-fecha')[:2]
	latest_pub = Publicacion.objects.filter(enportada=True,iddisponibilidad=4).order_by('-id')[:8]
	latest_blogs = Blog.objects.order_by('-fecha')[:2]
	latest_portafolio = Portafolio.objects.order_by('-fecha')[:6]
	latest_portada = FotoPortada.objects.order_by('orden')
	latest_service = Servicio.objects.filter(activo = True).order_by('-id')
	latest_aliados = Aliado.objects.order_by('-id')

	# imagenes = GaleriasImg.objects.order_by('-id')[:4]
	# videos = Video.objects.exclude(youtube__exact='').order_by('-id')[:4]
	# galerias = list(chain(imagenes,videos))
	galerias = GaleriasImg.objects.order_by('-id')[:8]

	# tematica_img = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	# tematicas_videos = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	# temas_galerias = list(set(chain(tematica_img,tematicas_videos)))
	temas_galerias = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
	# latest_videos = Video.objects.exclude(youtube__exact='').order_by('-id')[:6]
	proyectos = Proyecto.objects.order_by('-id')
	proy_exp = proyectos.filter(categoria = 'experiencia')
	proy_recientes = proyectos.filter(categoria = 'proyectos-recientes')

	beneficiarios = Proyecto.objects.aggregate(total = Sum('beneficiarios'))['total']
	total_publicaciones = Publicacion.objects.filter(iddisponibilidad=4).count()
	total_sistemas = Portafolio.objects.count()

	if request.method == 'POST':
		form = SuscripcionForm(request.POST)
		if form.is_valid():
			suscriptor = form.save(commit=False)
			suscriptor.save()
			mensaje = "Gracias por suscribirse a nuestro boletín"
	else:
		form = SuscripcionForm()

	return render(request, template, locals())


class ListNewsView(ListView):
	model = Noticia
	queryset = Noticia.objects.order_by('-fecha')
	# paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ListNewsView, self).get_context_data(**kwargs)
		context['foto_noticia'] = MenuFoto.objects.get(menu=4)
		context['categorias'] = Noticia.objects.values_list('categoria__slug','categoria__nombre').distinct('categoria').exclude(categoria = None)
		return context


def DetailNewsView(request, template='noticias/detalle_noticia.html', id=None, uri=None):
	nota = get_object_or_404(Noticia,id=id)
	try:
		lista_categoria = nota.categoria.all()[0]
		noticias_relacionadas = Noticia.objects.filter(categoria=lista_categoria).exclude(id=nota.id).order_by('-id')[:3]
		# nota_all = Noticia.objects.filter(idtematica=lista_tematicas).filter(tipo=2).exclude(id=nota.id).order_by('-fecha')[:3]

	except:
		pass

	foto_noticia = MenuFoto.objects.get(menu=4)

	return render(request, template, locals())


def contacto_ajax(request):
	if request.is_ajax():
		form = ContactForm(request.POST)
		if form.is_valid():
			subject = form.cleaned_data['asunto']
			message = form.cleaned_data['mensaje']
			sender = form.cleaned_data['correo']

			recipients = ['crocha09.09@gmail.com','manejo-informacion@simas.org.ni']

			send_mail(subject, message, sender, recipients)
			return HttpResponse( json.dumps( 'exito' ), mimetype='application/json' )
		else:
			return HttpResponse( json.dumps( 'falso' ), mimetype='application/json' )

def notas_categoria(request,template='noticias/noticia_list.html',slug=None):
	object_list = Noticia.objects.filter(categoria__slug=slug)
	categorias = Noticia.objects.values_list('categoria__slug','categoria__nombre').distinct('categoria').exclude(categoria = None)
	return render(request, template, locals())

# from haystack.query import SearchQuerySet
# def search_publicaciones(request, template="search/search.html"):
#     query = request.POST.get('s', '')
#     object_list1 = SearchQuerySet()

#     paginator = Paginator(object_list1, 20) # Show 25 contacts per page

#     page = request.GET.get('page')
#     try:
#         object_list = paginator.page(page)
#     except PageNotAnInteger:
#         # If page is not an integer, deliver first page.
#         object_list = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range (e.g. 9999), deliver last page of results.
#         object_list = paginator.page(paginator.num_pages)
#     return render(request,template,{'object_list': object_list, 'query': query})


#def sopa(texto):

#    soup = BeautifulSoup(texto)

#    for tag in soup.find_all('<p>&nbsp;</p>','<p>','<span>'):
#        tag.replaceWith('')

#    return soup.get_text()

#def funcion_limpiar(request):

#    texto = Noticia.objects.all()
#    for text in texto:
#         a = sopa(text.texto)
#         text.texto = a
#         text.save()
#    return 1

def get_deptos(request):
	id = request.GET.get('id', '')
	lista = []
	proyectos = Proyecto.objects.filter(departamento__codigo = id)
	for x in proyectos:
		lista.append((x.nombre,x.slug,x.get_thumb,x.beneficiarios))

	return HttpResponse(simplejson.dumps(list(lista)), content_type = 'application/json')

def get_img_servicios(request):
	id = request.GET.get('id', '')
	lista = []
	img = Servicio.objects.filter(id = id)
	for x in img:
		lista.append(x.get_thumb)
	return HttpResponse(simplejson.dumps(list(lista)), content_type = 'application/json')

from django.utils.html import *
from django.utils.text import Truncator

def get_blogs(request):
	id = request.GET.get('id', '')
	lista = []
	blog = Blog.objects.filter(id = id)
	for x in blog:
		resumen = strip_tags(Truncator(x.resumen).words(15))
		lista.append((x.blog,resumen,x.id,x.uri))
	return HttpResponse(simplejson.dumps(list(lista)), content_type = 'application/json')

from haystack.query import SearchQuerySet
from haystack.views import SearchView, search_view_factory
from haystack.forms import ModelSearchForm
def search_notas(request):
	sqs = SearchQuerySet().models(Noticia).order_by('-id')
	view = search_view_factory(
		view_class=SearchView,
		template='search/search_notas.html',
		searchqueryset=sqs,
		form_class=ModelSearchForm,
		results_per_page=16
		)
	return view(request)
